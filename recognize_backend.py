import mediapipe as mp
import numpy as np
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
import logging
from PIL import Image
from PIL import PngImagePlugin
from pydantic import BaseModel
import io
from fastapi import FastAPI
from fastapi import HTTPException
import base64
import cv2

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Create a GestureRecognizer object.
base_options = python.BaseOptions(model_asset_path='gesture_recognizer.task')
#options = vision.GestureRecognizerOptions(base_options=base_options)
#recognizer = vision.GestureRecognizer.create_from_options(options)
options = vision.GestureRecognizerOptions(base_options=base_options,
                                          running_mode=vision.RunningMode.IMAGE,
                                          num_hands=1,
                                          min_hand_detection_confidence=0.5,
                                          min_hand_presence_confidence=0.5,
                                          min_tracking_confidence=0.5)
recognizer = vision.GestureRecognizer.create_from_options(options)


# define the data model for request
class ImageData(BaseModel):
    detected_gesture: str
    detected_at: str
    image: bytes


app = FastAPI()


@app.post("/api/images")
async def handle_images(data: ImageData):
    # logging.info('Body: %s', data)

    gesture = data.detected_gesture
    detected_at = data.detected_at
    image_data = base64.b64decode(data.image)

    if not gesture or not detected_at or not image_data:
        raise HTTPException(status_code=400, detail="Invalid data provided")

    # Log the data
    logging.info('Gesture image received with pre-classfication: %s at %s', gesture, detected_at)

    # Run process_image method asynchronously
    await process_image(image_data, gesture, detected_at)

    return {"Success": True}


async def process_image(image_data, suggested_gesture, detected_at):
    # Convert the binary data to an image
    image_np = np.frombuffer(image_data, dtype=np.uint8)
    raw_image = cv2.imdecode(image_np, cv2.IMREAD_UNCHANGED)
    raw_image_rgb = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    # Save the raw image (or processed image in this case)
    # cv2.imwrite('/app/images-output/raw_image.png', raw_image_rgb)
    image = mp.Image(image_format=mp.ImageFormat.SRGB, data=raw_image_rgb)

    # process_hands(raw_image_rgb)
    recognition_result = recognizer.recognize(image)
    category_name = 'none'
    gestures = recognition_result.gestures
    # Get gesture classification results
    if gestures and gestures[0]:
        gesture = gestures[0]
    category_name = gesture[0].category_name
    score = round(gesture[0].score, 2)
    result_text = f'{category_name} ({score})'
    logging.info('Gesture suggested: ' + suggested_gesture + ', calculated as: ' + result_text + " - at " + detected_at)


def process_hands(raw_image_rgb):
    # Initialize MediaPipe Hands.
    with mp.solutions.hands.Hands() as hands:
        # Process the image
        results = hands.process(raw_image_rgb)

    # Check if any hand is detected
    if results.multi_hand_landmarks:
        for hand_landmarks in results.multi_hand_landmarks:
            print('Hand landmarks:', hand_landmarks)




if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=80)
