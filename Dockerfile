# Use an official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.10.12-slim

# Set the working directory in the container to /app.
WORKDIR /app
# Copy the current directory contents into the container at /app.
COPY ./requirements-docker.txt /app

# Install build essentials
RUN apt-get update && apt-get install -y build-essential
RUN apt-get install -y ffmpeg libsm6 libxext6
# Update pip
RUN pip install --upgrade pip
# Upgrade important libs
RUN pip install setuptools wheel
# Install any needed packages specified in requirements.txt
RUN pip install opencv-python-headless
RUN pip install --trusted-host pypi.python.org -r requirements-docker.txt
# Install FasAPI dependencies
RUN pip install fastapi uvicorn
RUN pip install pydantic
RUN mkdir /app/images-out
RUN chmod 777 images-out

COPY ./recognize_backend.py /app
COPY ./gesture_recognizer.task /app

# Make port 80 available to the world outside this container.
EXPOSE 80

# Run recognize.py when the container launches.
CMD ["python", "./recognize_backend.py"]