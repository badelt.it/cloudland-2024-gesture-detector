#!/usr/bin/env python3

import time
import os
import requests
from datetime import datetime
import base64
import json

# Watched directory
watched_dir = "./output"
last_files = set()

while True:
    current_files = set(os.listdir(watched_dir))

    new_files = current_files - last_files
    for filename in new_files:
        if filename.endswith('.png'):
            # Step 2: store the prefix of a new file
            gesture_detected = filename.split('_')[0]

            # Step 3: take the current time
            detected_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            # Step 4: create a JSON map
            with open(os.path.join(watched_dir, filename), "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read()).decode('utf-8')

            data = {
                "detected_gesture": gesture_detected,
                "detected_at": detected_at,
                "image": encoded_string
            }

            # Step 5: make an HTTP POST request
            response = requests.post("http://localhost:8080/api/images", json=data)

            # Check response status
            if response.status_code != 200:
                print(f"Unable to send POST request, status code: {response.status_code}")

    last_files = current_files
    # Wait for 1 second before next check
    time.sleep(1)
