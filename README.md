# MediaPipe Object Detection example with Raspberry Pi

This gesture-detector is based on the *object*_detection example from [MediaPipe](https://github.com/google/mediapipe). 
Please see that documentation for the foundations. The detector is a Python script for the 
Raspberry Pi to perform real-time object detection using images streamed from the Pi 
Camera. It draws a bounding box around each detected "object" (gesture) in the camera
preview (when the object score is above a given threshold).

The original example was modified to ues picamera2 instead of the camera access code from CV2.

## Set up your hardware
Before you begin, you need to
[set up your Raspberry Pi](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up)
with Raspberry 64-bit Pi OS (preferably updated to Buster).

You also need to [connect and configure the Pi Camera](
https://www.raspberrypi.org/documentation/configuration/camera.md) if you use
the Pi Camera. This code also works with USB camera connect to the Raspberry Pi.

And to see the results from the camera, you need a monitor connected
to the Raspberry Pi. It's okay if you're using SSH to access the Pi shell
(you don't need to use a keyboard connected to the Pi)—you only need a monitor
attached to the Pi to see the camera stream.

## Installing dependencies
```
sudo apt install -y python3-picamera2
python3 -m venv .venv --system-site-packages
source .venv/bin/activate
pip install mediapipe
```

## Run the recognizer
There are two variants.
The first one is a general object detector, using a model that was fine-tuned to recognize a few hand 
gestures:
```
python3 detect.py \
  --model model.tflite
```
The second variant will normally provide better results here as it a specialized hand gesture recognizer, 
that is able to recognize and visualize hands with all their defining parts, in particular the different 
fingers and their sections. It uses a model that was again fine-tuned to recognize a few special gestures 
on top of the standard ones (in particular "rock" aka "horns" and "cloudland" - "horns" turned by 90 
degrees to symbolize a C).
```
python3 recognize.py \
  --model gesture_recognizer.task
```



You should see the camera feed appear on the monitor attached to your Raspberry
Pi. Show some gestures in front of the camera, and depending on the variant you use
you'll see boxes drawn around those that the model recognizes, or a representation 
of the hand and fingers using coloured dots and lines between them. The category
label (e.g. "thumbs_up" or "rock") and a confidence score will be displayed as well. 

It also prints the number of frames per second (FPS)
at the top-left corner of the screen. As the pipeline contains some processes
other than model inference, including visualizing the detection results, you can
expect a higher FPS if your inference pipeline runs in headless mode without
visualization.


Whenever a gesture was detected, a visualized "flash" (blank screen for a split-second)
will show that the corresponding video frame was saved as a jpeg image in an output folder
together with its categorization. After each save, the process will wait for at least
10 seconds until saving another frame, to prevent continuous saving and thus "flooding"
if the output folder.

_ToDo: Modify this to use a similarity metric between frames rather than a fixed wait time
(only when the frame with a recognized gesture is sufficiently different from the last one
will the new frame be saved)._

_ToDo: Add background process (separate python script to watch output folder and process 
the images further (e.g. upload them to a backend)._

**Parameters for detect.py**

*   You can optionally specify the `model` parameter to set the TensorFlow Lite
    model to be used:
    *   The default value is `efficientdet_lite0.tflite`
    *   TensorFlow Lite object detection models **with metadata**  
        * Models from [MediaPipe Models](https://developers.google.com/mediapipe/solutions/vision/object_detector/index#models)
        * Models trained with [MediaPipe Model Maker](https://developers.google.com/mediapipe/solutions/customization/object_detector) are supported.
*   You can optionally specify the `maxResults` parameter to limit the list of
    detection results:
    *   Supported value: A positive integer.
    *   Default value: `5`
*   You can optionally specify the `scoreThreshold` parameter to adjust the
    score threshold of detection results:
    *   Supported value: A floating-point number.
    *   Default value: `0.25`.
*   Example usage:
    ```
    python3 detect.py \
      --model efficientdet_lite0.tflite \
      --maxResults 5 \
      --scoreThreshold 0.3
    ```

**Parameters for recognize.py**

+*   You can optionally specify the `model` parameter to set the task file to be used:
    *   The default value is `gesture_recognizer.task`
    *   TensorFlow Lite gesture recognizer models **with metadata**  
        * Models from [MediaPipe Models](https://developers.google.com/mediapipe/solutions/vision/gesture_recognizer#models)
        * Custom models trained with [MediaPipe Model Maker](https://developers.google.com/mediapipe/solutions/vision/gesture_recognizer#custom_models) are supported.
*   You can optionally specify the `numHands` parameter to the maximum 
    number of hands that can be detected by the recognizer:
    *   Supported value: A positive integer (1-2)
    *   Default value: `1`
*   You can optionally specify the `minHandDetectionConfidence` parameter to adjust the
    minimum confidence score for hand detection to be considered successful:
    *   Supported value: A floating-point number.
    *   Default value: `0.5`
*   You can optionally specify the `minHandPresenceConfidence` parameter to adjust the 
    minimum confidence score of hand presence score in the hand landmark detection:
    *   Supported value: A floating-point number.
    *   Default value: `0.5`
*   You can optionally specify the `minTrackingConfidence` parameter to adjust the 
    minimum confidence score for the hand tracking to be considered successful:
    *   Supported value: A floating-point number.
    *   Default value: `0.5`
*   Example usage:
    ```
    python3 recognize.py \
      --model gesture_recognizer.task \
      --numHands 2 \
      --minHandDetectionConfidence 0.5
    ```

## OCI Container version of recognizer

There is also a headless / backend version of the recognizer available, see ``recognize_backend.py`` and the Dockerfile.
It will accept a JSON of the following form at ``/api/images``:
```
{ 
  "detected_gesture": "<gesture-name, e.g.: rock>", 
  "detected_at": "<timestamp>", 
  "image": "<base64-encoded image byte data>"
}
```
The received images will - for now - only be used to run the same inference process, 
re-categorizing the image, and then logging the result like this: 

```('Gesture suggested: cloudland, calculated as: rock - at Do 20 Jun 2024 12:01:34 CEST)```

The ```send_to_backend.py``` script is meant to run on the Raspberry to watch the
output directory for any new images added by ``recognize.py``. It will then take these
images, create the JSON format above using the category name in the file name and the current time,
and POST it to the backend process at a URL (currently) hardcoded into the script. 
